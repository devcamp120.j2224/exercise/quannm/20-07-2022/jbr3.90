package com.devcamp.jbr390.jbr390;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RectangleController {
    @CrossOrigin
    @GetMapping("/rectangle-area")
    public double getAreaRec() {
        Rectangle rec = new Rectangle(2.5f, 3.0f);
        return rec.getArea();
    }

    @CrossOrigin
    @GetMapping("/rectangle-perimeter")
    public double getPerimeterRec() {
        Rectangle rec = new Rectangle(2.5f, 3.0f);
        return rec.getPerimeter();
    }
}
